import time
import RPi.GPIO as GPIO
from enum import Enum

class Motor_state(Enum):
    MOTOROFF = 0
    MOTORSTATE1 = 1
    MOTORSTATE2 = 2
    MOTORSTATE3 = 3
    MOTORSTATE4 = 4
    
class Rotation_dir(Enum):
    clockwise = 0
    counterclockwise = 1

class Stepper_motor:

    # pin numbers in BCM format
    MOTOR1PIN1 = 5  # GPIO21
    MOTOR1PIN2 = 6  # GPIO22
    MOTOR1PIN3 = 13 # GPIO23
    MOTOR1PIN4 = 19 # GPIO24
    
    full_rotation_steps = 2048
    angle_by_step = 360.0/2048.0
    delay = 0.01 # 10 ms default

    motor1State = Motor_state(Motor_state.MOTOROFF)

    def __init__(self, delay = 0.01):
        self.delay = delay
        
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.MOTOR1PIN1, GPIO.OUT)
        GPIO.setup(self.MOTOR1PIN2, GPIO.OUT)
        GPIO.setup(self.MOTOR1PIN3, GPIO.OUT)
        GPIO.setup(self.MOTOR1PIN4, GPIO.OUT)

        GPIO.setup(26, GPIO.IN) # Hall sensor
        
        self.resetPosition()
        
    def motorService(self):
        self.clearMotorPins()
        if self.motor1State == Motor_state.MOTORSTATE1:
            GPIO.output(self.MOTOR1PIN1, GPIO.HIGH)
            GPIO.output(self.MOTOR1PIN2, GPIO.HIGH)
            
        elif self.motor1State == Motor_state.MOTORSTATE2:
            GPIO.output(self.MOTOR1PIN2, GPIO.HIGH)
            GPIO.output(self.MOTOR1PIN3, GPIO.HIGH)
            
        elif self.motor1State == Motor_state.MOTORSTATE3:
            GPIO.output(self.MOTOR1PIN3, GPIO.HIGH)
            GPIO.output(self.MOTOR1PIN4, GPIO.HIGH)
            
        elif self.motor1State == Motor_state.MOTORSTATE4:
            GPIO.output(self.MOTOR1PIN4, GPIO.HIGH)
            GPIO.output(self.MOTOR1PIN1, GPIO.HIGH)


    def nextMotorState(self, direction):
        newMotorState = Motor_state(Motor_state.MOTOROFF)

        if Rotation_dir.counterclockwise == direction: 		
            if self.motor1State == Motor_state.MOTORSTATE1:
                newMotorState = Motor_state.MOTORSTATE2
                
            elif self.motor1State == Motor_state.MOTORSTATE2:
                newMotorState = Motor_state.MOTORSTATE3
                
            elif self.motor1State == Motor_state.MOTORSTATE3:
                newMotorState = Motor_state.MOTORSTATE4
                
            elif self.motor1State == Motor_state.MOTORSTATE4:
                newMotorState = Motor_state.MOTORSTATE1
                
            elif self.motor1State == Motor_state.MOTOROFF:
                newMotorState = Motor_state.MOTORSTATE1
                
            else:
                newMotorState = Motor_state.MOTOROFF

        else:				
            if self.motor1State == Motor_state.MOTORSTATE1:
                newMotorState = Motor_state.MOTORSTATE4
                
            elif self.motor1State == Motor_state.MOTORSTATE2:
                newMotorState = Motor_state.MOTORSTATE1
            
            elif self.motor1State == Motor_state.MOTORSTATE3:
                newMotorState = Motor_state.MOTORSTATE2
                
            elif self.motor1State == Motor_state.MOTORSTATE4:
                newMotorState = Motor_state.MOTORSTATE3
                
            elif self.motor1State == Motor_state.MOTOROFF:
                newMotorState = Motor_state.MOTORSTATE1
                
            else:
                newMotorState = Motor_state.MOTOROFF

        self.motor1State = newMotorState
        self.motorService()
        

    def turnMotorOff(self):
            motor1State = Motor_state.MOTOROFF
            self.motorService()
        
        
    def clearMotorPins(self):
            GPIO.output(self.MOTOR1PIN1, GPIO.LOW)
            GPIO.output(self.MOTOR1PIN2, GPIO.LOW)
            GPIO.output(self.MOTOR1PIN3, GPIO.LOW)
            GPIO.output(self.MOTOR1PIN4, GPIO.LOW)


    def rotateAngle(self, angle):
        numberOfSteps = 0
        direction = Rotation_dir.counterclockwise
        if angle < 0.0:
            angle = -angle
            direction = Rotation_dir.counterclockwise

        else:
            direction = Rotation_dir.clockwise

        numberOfSteps = int((self.full_rotation_steps*angle)/360.0)
        for step in range(numberOfSteps):
            self.nextMotorState(direction)
            time.sleep(self.delay)

        self.turnMotorOff()

    def resetPosition(self):

        while GPIO.input(26) == 1:
            self.rotateAngle(1)

if __name__ == "__main__":

    try:
        mymotor = Stepper_motor(0.0174) # make full rotation take 36 seconds
        startTime = time.time()
        mymotor.rotateAngle(360)
        print("--- %s seconds ---" % (time.time() - startTime))
        #mymotor.resetPosition()


    finally:
        print("Cleaning up")
        GPIO.cleanup()

