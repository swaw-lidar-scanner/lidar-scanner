#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"

#define VERSION_REQUIRED_MAJOR 1
#define VERSION_REQUIRED_MINOR 0
#define VERSION_REQUIRED_BUILD 2

void print_pal_error(VL53L0X_Error Status){
    char buf[VL53L0X_MAX_STRING_LENGTH];
    VL53L0X_GetPalErrorString(Status, buf);
    printf("API Status: %i : %s\n", Status, buf);
}

int main(int argc, char **argv) {
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    VL53L0X_Dev_t MyDevice;
    VL53L0X_Dev_t *pMyDevice = &MyDevice;
    VL53L0X_Version_t Version;
    VL53L0X_Version_t *pVersion = &Version;
    VL53L0X_DeviceInfo_t DeviceInfo;
    int32_t status_int;
    FixPoint1616_t XTalkCalDistance = 100*65536;
    FixPoint1616_t XTalkCompensationRateMegaCps;
    uint8_t VhvSettings;
    uint8_t PhaseCal;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;

    pMyDevice->I2cDevAddr = 0x29;
    pMyDevice->fd = VL53L0X_i2c_init("/dev/i2c-1", pMyDevice->I2cDevAddr);
    if (MyDevice.fd<0) {
        Status = VL53L0X_ERROR_CONTROL_INTERFACE;
        printf ("Failed to init\n");
    }

    if(Status == VL53L0X_ERROR_NONE){
        status_int = VL53L0X_GetVersion(pVersion);
        if (status_int != 0)
            Status = VL53L0X_ERROR_CONTROL_INTERFACE;
    }

    if(Status == VL53L0X_ERROR_NONE){
        if( pVersion->major != VERSION_REQUIRED_MAJOR ||
            pVersion->minor != VERSION_REQUIRED_MINOR ||
            pVersion->build != VERSION_REQUIRED_BUILD ){
            printf("VL53L0X API Version Error: Your firmware has %d.%d.%d (revision %d). This example requires %d.%d.%d.\n",
                pVersion->major, pVersion->minor, pVersion->build, pVersion->revision,
                VERSION_REQUIRED_MAJOR, VERSION_REQUIRED_MINOR, VERSION_REQUIRED_BUILD);
        }
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_DataInit\n");
        Status = VL53L0X_DataInit(&MyDevice);
        print_pal_error(Status);
    }
    
    if(Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_GetDeviceInfo(&MyDevice, &DeviceInfo);
    }
    
    if(Status == VL53L0X_ERROR_NONE) {
        if ((DeviceInfo.ProductRevisionMinor != 1) && (DeviceInfo.ProductRevisionMinor != 1)) {
        	printf("Error expected cut 1.1 but found cut %d.%d\n",
        			DeviceInfo.ProductRevisionMajor, DeviceInfo.ProductRevisionMinor);
        	Status = VL53L0X_ERROR_NOT_SUPPORTED;
        }
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_StaticInit\n");
        Status = VL53L0X_StaticInit(pMyDevice);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_PerformRefSpadManagement\n");
        Status = VL53L0X_PerformRefSpadManagement(pMyDevice, &refSpadCount, &isApertureSpads);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_PerformRefCalibration\n");
        Status = VL53L0X_PerformRefCalibration(pMyDevice, &VhvSettings, &PhaseCal);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {   
        printf("Call if VL53L0X_PerformXTalkCalibration\n");
        Status = VL53L0X_PerformXTalkCalibration(pMyDevice, XTalkCalDistance, &XTalkCompensationRateMegaCps);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        FILE *filePointer; 
        filePointer = fopen("xtalk.txt", "w");
        if ( filePointer == NULL ){
            printf("xtalk.txt file failed to open.");
        } else {
            printf("%u", XTalkCompensationRateMegaCps);
            fprintf(filePointer, "%u", XTalkCompensationRateMegaCps);
            fclose(filePointer);
        }
    }

    printf ("Close Comms\n");
    VL53L0X_i2c_close();
    print_pal_error(Status);

    return 0;
}
