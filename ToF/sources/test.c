#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"

#define VERSION_REQUIRED_MAJOR 1
#define VERSION_REQUIRED_MINOR 0
#define VERSION_REQUIRED_BUILD 2


void print_pal_error(VL53L0X_Error Status) {
    char buf[VL53L0X_MAX_STRING_LENGTH];
    VL53L0X_GetPalErrorString(Status, buf);
    printf("API Status: %i : %s\n", Status, buf);
}

void getCalValues(int32_t *offset, FixPoint1616_t *xtalk){
    FILE *offsetFile = fopen("offset.txt", "r");
    fscanf(offsetFile, "%d", offset);
    fclose(offsetFile);

    FILE *xtalkFile = fopen("xtalk.txt", "r");
    fscanf(xtalkFile, "%d", xtalk);
    fclose(xtalkFile);
}
 
VL53L0X_Error test(VL53L0X_Dev_t *pMyDevice) {
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;
    int8_t start;

    FixPoint1616_t XTalkCalDistance = 100;
    FixPoint1616_t XTalkCompensationRateMegaCps;
    FixPoint1616_t CalDistanceMilliMeter = 100;
    int32_t OffsetMicroMeter;

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_StaticInit\n");
        Status = VL53L0X_StaticInit(pMyDevice);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_PerformRefSpadManagement\n");
        Status = VL53L0X_PerformRefSpadManagement(pMyDevice,
        		&refSpadCount, &isApertureSpads);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_PerformRefCalibration\n");
        Status = VL53L0X_PerformRefCalibration(pMyDevice,
        		&VhvSettings, &PhaseCal);
        print_pal_error(Status);
    }
    
    getCalValues(&OffsetMicroMeter, &XTalkCompensationRateMegaCps);
    
    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_SetXTalkCompensationRateMegaCps\n");
        //Status = VL53L0X_SetXTalkCompensationRateMegaCps(pMyDevice, XTalkCompensationRateMegaCps);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_SetOffsetCalibrationDataMicroMeter\n");
        //Status = VL53L0X_SetOffsetCalibrationDataMicroMeter(pMyDevice, OffsetMicroMeter);
        print_pal_error(Status);
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_SetXTalkCompensationEnable\n");
        Status = VL53L0X_SetXTalkCompensationEnable(pMyDevice, 1);
        print_pal_error(Status);
    }

    FixPoint1616_t XTalkComp;
    int32_t OffsetMic;

    VL53L0X_GetXTalkCompensationRateMegaCps(pMyDevice, &XTalkComp);
    printf("\n%d\n", XTalkComp);

    VL53L0X_GetOffsetCalibrationDataMicroMeter(pMyDevice, &OffsetMic);
    printf("\n%d\n", OffsetMic);

    if(Status == VL53L0X_ERROR_NONE)
	Status = VL53L0X_ClearInterruptMask(pMyDevice, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);

    return Status;
}


int main(int argc, char **argv) {
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    VL53L0X_Dev_t MyDevice;
    VL53L0X_Dev_t *pMyDevice = &MyDevice;
    VL53L0X_Version_t Version;
    VL53L0X_Version_t *pVersion = &Version;
    VL53L0X_DeviceInfo_t DeviceInfo;
    int32_t status_int;

    // Initialize Comms
    pMyDevice->I2cDevAddr = 0x29;
    pMyDevice->fd = VL53L0X_i2c_init("/dev/i2c-1", pMyDevice->I2cDevAddr); //choose between i2c-0 and i2c-1; On the raspberry pi zero, i2c-1 are pins 2 and 3
    if (MyDevice.fd<0) {
        Status = VL53L0X_ERROR_CONTROL_INTERFACE;
        printf ("Failed to init\n");
    }

     //Get the version of the VL53L0X API running in the firmware
    if(Status == VL53L0X_ERROR_NONE) {
        status_int = VL53L0X_GetVersion(pVersion);
        if (status_int != 0)
            Status = VL53L0X_ERROR_CONTROL_INTERFACE;
    }

    //Verify the version of the VL53L0X API running in the firmrware
    if(Status == VL53L0X_ERROR_NONE) {
        if( pVersion->major != VERSION_REQUIRED_MAJOR ||
            pVersion->minor != VERSION_REQUIRED_MINOR ||
            pVersion->build != VERSION_REQUIRED_BUILD ) {
            printf("VL53L0X API Version Error: Your firmware has %d.%d.%d (revision %d). This example requires %d.%d.%d.\n",
                pVersion->major, pVersion->minor, pVersion->build, pVersion->revision,
                VERSION_REQUIRED_MAJOR, VERSION_REQUIRED_MINOR, VERSION_REQUIRED_BUILD);
        }
    }

    if(Status == VL53L0X_ERROR_NONE) {
        printf ("Call of VL53L0X_DataInit\n");
        Status = VL53L0X_DataInit(&MyDevice);
        print_pal_error(Status);
    }
    
    if(Status == VL53L0X_ERROR_NONE) {
        Status = VL53L0X_GetDeviceInfo(&MyDevice, &DeviceInfo);
        print_pal_error(Status);
    }
    
    if(Status == VL53L0X_ERROR_NONE) {
        printf("VL53L0X_GetDeviceInfo:\n");
        printf("Device Name : %s\n", DeviceInfo.Name);
        printf("Device Type : %s\n", DeviceInfo.Type);
        printf("Device ID : %s\n", DeviceInfo.ProductId);
        printf("ProductRevisionMajor : %d\n", DeviceInfo.ProductRevisionMajor);
        printf("ProductRevisionMinor : %d\n", DeviceInfo.ProductRevisionMinor);

        if ((DeviceInfo.ProductRevisionMinor != 1) && (DeviceInfo.ProductRevisionMinor != 1)) {
        	printf("Error expected cut 1.1 but found cut %d.%d\n",
        			DeviceInfo.ProductRevisionMajor, DeviceInfo.ProductRevisionMinor);
        	Status = VL53L0X_ERROR_NOT_SUPPORTED;
        }
    }

    if(Status == VL53L0X_ERROR_NONE) {
        Status = test(pMyDevice);
        print_pal_error(Status);
    }

    printf ("Close Comms\n");
    VL53L0X_i2c_close();
    print_pal_error(Status);
    
    return (0);
}

