#import pandas as pd
import matplotlib.pyplot as plt
import math
import os

script_dir = os.path.dirname(__file__)
rel_path = './results.txt'
abs_file_path = os.path.join(script_dir, rel_path)

def read_scan():
    scan_df = []
    with open(abs_file_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line.strip():
                scan_df.append(int(line))
    f.close
    #scan_df = pd.read_csv("scan/worek_1.txt", engine="python", sep=": ", index_col=0, names=["nr", "radius"])
    return scan_df


def polar_visualisation(coords_dict):
    angles_list = []
    radius_list = []
    for value in coords_dict:
        angles_list.append(value)
        radius_list.append(coords_dict[value])

    print(angles_list)
    print(radius_list)
    plt.axes(projection='polar')

    plt.plot(angles_list, radius_list, '.')

    plt.show()


if __name__ == "__main__":
    zero_point_radius = 83  # In millimeters

    scan = read_scan()
    nr_of_elements = len(scan)

    for i in range(len(scan)):
        scan[i] = zero_point_radius - scan[i]
    print(scan)

    radius_angles_dict = dict()
    angle = 0
    for radius in scan:
        # Dict_name[key] = value
        radius_angles_dict[angle] = radius
        angle += 2*math.pi/len(scan)


    print(radius_angles_dict)

    polar_visualisation(radius_angles_dict)
